package pl.sda.strategy;

public interface TextFormatterStrategy {

    String format(String text);

}



