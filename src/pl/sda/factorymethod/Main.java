package pl.sda.factorymethod;

public class Main {

    public static void main(String[] args) {

        Employee programmer =
                EmployeeFactory.createEmployee("Michał", "Suwała", EmployeeType.PROGRAMMER);

        Employee tester =
                EmployeeFactory.createEmployee("Michał", "Nowak", EmployeeType.TESTER);

        Employee architect =
                EmployeeFactory.createEmployee("Anna", "Kowalska", EmployeeType.ARCHITECT);


        Employee employeeList[] = {programmer, tester, architect};

        for (Employee employee: employeeList) {
            System.out.println(employee.getName() + " " + employee.getSurname()
                    + " jako " + employee.getType() + " zarabia: " + employee.calculateSalary());
        }

    }

}
