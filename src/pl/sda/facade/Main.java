package pl.sda.facade;

public class Main {

    public static void main(String[] args) {

        ATMFacade facade = new ATMFacade();
        int pin[] = {1,2,3,4};
        facade.withdrawal("4321", "PKO", pin, 1000);

    }

}
