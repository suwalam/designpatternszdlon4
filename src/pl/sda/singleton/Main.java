package pl.sda.singleton;

public class Main {

    public static void main(String[] args) {

        Singleton1 singleton1 = Singleton1.INSTANCE;

        singleton1.doSth();

        System.out.println(Singleton1.INSTANCE);
        System.out.println(Singleton1.INSTANCE);
        System.out.println(Singleton1.INSTANCE);

        Singleton2 singleton2 = Singleton2.getInstance();

        singleton2.doSth();

        System.out.println(Singleton2.getInstance());
        System.out.println(Singleton2.getInstance());
        System.out.println(Singleton2.getInstance());

        LogWriterSingleton logWriterSingleton = LogWriterSingleton.getInstance();

        logWriterSingleton.log("ala ma kota");
        logWriterSingleton.log("a kot ma ale");
    }

}
