package pl.sda.observer;

import java.util.List;

public interface Observer {

    void update(List<Integer> result);

}
