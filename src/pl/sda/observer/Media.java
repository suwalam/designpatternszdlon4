package pl.sda.observer;

import java.util.ArrayList;
import java.util.List;

public abstract class Media implements Observer {

    private Totolotek totolotek;

    private List<Integer> resultFromTotolotek = new ArrayList<>();

    public Media(Totolotek totolotek) {
        this.totolotek = totolotek;
    }

    @Override
    public void update(List<Integer> result) {
        resultFromTotolotek.clear();

        for (Integer integer : result) {
            resultFromTotolotek.add(integer);
        }
    }

    public void inform() {
        System.out.println(resultFromTotolotek);
    }

    public void unsubscribe() {
        totolotek.removeObserver(this);
        resultFromTotolotek.clear();
    }
}
