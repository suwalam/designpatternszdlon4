package pl.sda.decorator;

public class PineaplePizzaDecorator extends AbstractPizzaDecorator {


    public PineaplePizzaDecorator(AbstractPizza abstractPizza) {
        super(abstractPizza);
    }

    @Override
    public double price() {
        return abstractPizza.price() + 4.0;
    }

    @Override
    public String getDescription() {
        return abstractPizza.getDescription() + ", ananas";
    }
}
