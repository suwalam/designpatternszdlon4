package pl.sda.decorator;

public class Main {

    public static void main(String[] args) {

        AbstractPizza margarita = new MargaritaPizza();

        AbstractPizza hamPizza = new HamPizzaDecorator(margarita);
        System.out.println(hamPizza.getDescription());
        System.out.println(hamPizza.price());

        AbstractPizza hawaianPizza = new PineaplePizzaDecorator(hamPizza);

        System.out.println(hawaianPizza.getDescription());
        System.out.println(hawaianPizza.price());

        SDACouponPizzaDecorator sdaCoupon = new SDACouponPizzaDecorator(hawaianPizza, "SDA2020");
        System.out.println(sdaCoupon.getDescription());
        System.out.println(sdaCoupon.price());


    }

}
