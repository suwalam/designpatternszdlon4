package pl.sda.decorator;

public class SDACouponPizzaDecorator extends AbstractPizzaDecorator {

    private String coupon;

    private int percent;

    private boolean isValidCoupon;

    public SDACouponPizzaDecorator(AbstractPizza abstractPizza, String coupon) {
        super(abstractPizza);
        this.coupon = coupon;
        this.percent = 20;
        this.isValidCoupon = validateCouponAndDescription();
    }

    @Override
    public double price() {
        if (isValidCoupon) {
            double originalPrice = abstractPizza.price();
            return originalPrice - (originalPrice * (percent/100.0));
        }

        return abstractPizza.price();
    }

    @Override
    public String getDescription() {
        if (isValidCoupon) {
            return abstractPizza.getDescription() + ", zniżka " + percent + "%, kupon " + coupon;
        }

        return abstractPizza.getDescription();
    }

    private boolean validateCouponAndDescription() {
        return this.coupon.equals("SDA2020")
                && abstractPizza.getDescription().contains("ananas");

    }
}
