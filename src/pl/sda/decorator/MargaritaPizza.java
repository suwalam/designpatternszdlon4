package pl.sda.decorator;

public class MargaritaPizza extends AbstractPizza {

    public MargaritaPizza() {
        this.description = "sos";
    }

    @Override
    public double price() {
        return 15;
    }
}
