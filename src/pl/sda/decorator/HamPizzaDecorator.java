package pl.sda.decorator;

public class HamPizzaDecorator extends AbstractPizzaDecorator {

    public HamPizzaDecorator(AbstractPizza abstractPizza) {
        super(abstractPizza);
    }

    @Override
    public double price() {
        return abstractPizza.price() + 3.0;
    }

    @Override
    public String getDescription() {
        return abstractPizza.getDescription() + ", szynka";
    }
}
