package pl.sda.adapter;

public interface Led {

    void turnOnLed();

    void turnOffLed();

}
