package pl.sda.adapter;

public class LedImpl implements Led {

    @Override
    public void turnOnLed() {
        System.out.println("Włączam LED");
    }

    @Override
    public void turnOffLed() {
        System.out.println("Wyłączam LED");
    }
}
