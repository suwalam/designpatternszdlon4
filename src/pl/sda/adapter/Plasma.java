package pl.sda.adapter;

public interface Plasma {

    void turnOnPlasma();

    void turnOffPlasma();

}
