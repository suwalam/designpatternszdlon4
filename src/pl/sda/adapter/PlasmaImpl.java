package pl.sda.adapter;

public class PlasmaImpl implements Plasma {

    @Override
    public void turnOnPlasma() {
        System.out.println("Włączam plasmę");
    }

    @Override
    public void turnOffPlasma() {
        System.out.println("Wyłączam plasmę");
    }
}
