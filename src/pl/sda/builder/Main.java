package pl.sda.builder;

public class Main {

    public static void main(String[] args) {

        Person.Builder builder = new Person.Builder();



        Person person = builder
                .withName("Michał")
                .withSurname("Suwała")
                .withAddress("Warszawa")
                .withAge(30)
                .build();

        System.out.println(person);

        Person person2 = builder
                .withHeight(185)
                .build();

        System.out.println(person2);


    }

}
